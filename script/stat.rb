#!/usr/bin/env ruby

require 'socket'
require 'redis'

ENV["RAILS_ENV"] ||= "development"

#require File.dirname(__FILE__) + "/../config/environment"
require File.expand_path("../../config/environment", __FILE__)

$running = true
Signal.trap("TERM") do
  $running = false
end

p "init"

sock = UDPSocket.new
sock.bind(nil, 3001)

redis = Redis.new

p "started"
while($running) do
  data, sender = sock.recvfrom(32)
  p data, sender
  uid, amount, sess_id = data.split(":")
#  p uid, amount, sess_id
#  p (user = User.find(uid))
#  p user.sess_id, sess_id
  if sess_id and (user = User.find(uid)) and user.sess_id == sess_id
    redis.incrby("user:"+uid, amount.to_i)
  end
end
