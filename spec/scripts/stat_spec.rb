require 'spec_helper'
require 'socket'
require 'redis'


  describe "receive" do
    
#    after(:each) do
#      
#    end
    
    def create_user
      u = User.create(
        name: "somename",
        email: "some@mail.com",
        password: "passwd",
        sess_id: "someid"
      )
      p u.errors
      u.id
    end
    
    context "with valid data" do
      it "should increase" do
        redis = Redis.new
        s = UDPSocket.new
        uid = create_user.to_s
        s.send(uid+":10:someid", 0, 'localhost', 3001)
        p "---",uid,redis.get("user:"+uid)
        redis.get("user:"+uid).should eq("10")
        User.find(uid).delete
      end
    end
    
#    context "with invalid data" do
#      it "should NOT increase" do
#        expect {
#          post_user_data("sm")
#        }.to change { User.count }.by(0)
#      end
#    end
    
  end


