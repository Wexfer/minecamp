require 'spec_helper'

describe UsersController do
  describe "create" do
    
    def post_user_data(n="somename", m="some@mail.com", p="passwd")
      post :create,
        user: {
          name: n,
          email: m,
          password: p
        }
    end
    
    context "with valid data" do
      it "should register user" do
        expect {
          post_user_data
        }.to change { User.count }.by(1)
      end
      it "should redirect after register" do
        expect {
          post_user_data
        }.to redirect_to("/uses")
      end
    end
    
    context "with invalid data" do
      it "should register user" do
        expect {
          post_user_data("sm")
        }.to change { User.count }.by(0)
      end
    end
    
  end
end

#TODO: "expect to redirect" after OK reg ???
#TODO: install Cucumber; check when request "/users.json", it has id, name, email

#    it "shows users" do
#      { :get => "/users" }.should route_to(:controller => "users", :action => "index")
#    end
#    
#    it "shows user create page" do
#      { :get => "/users/new" }.should route_to(:controller => "users", :action => "new")
#    end
