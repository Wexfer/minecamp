class BattleAddAdminAndStatus < ActiveRecord::Migration
  def change
    add_column :battles, :user_id, :integer
    add_column :battles, :status, :integer
  end
end
