class CreateLoots < ActiveRecord::Migration
  def change
    create_table :loots do |t|
      t.string :name
      t.integer :amount
      t.integer :generation_interval
      t.integer :min_pack_size
      t.integer :max_pack_size
      t.string :look_id

      t.timestamps
    end
  end
end
