class CreateCubes < ActiveRecord::Migration
  def change
    create_table :cubes do |t|
      t.string :name
      t.integer :health
      t.integer :generation_interval
      t.string :look_id

      t.timestamps
    end
  end
end
