class CreateBattleResults < ActiveRecord::Migration
  def change
    create_table :battle_results do |t|
      t.belongs_to :battle
      t.belongs_to :user
      t.integer :chips

      t.timestamps
    end
  end
end
