class UserAddGameParams < ActiveRecord::Migration
  def change
    add_column :users, :speed, :float
    add_column :users, :jump_height, :float
    add_column :users, :capacity, :float
    add_column :users, :weight, :float
    add_column :users, :expa, :integer
    add_column :users, :chips, :integer
  end
end
