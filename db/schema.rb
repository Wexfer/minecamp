# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130725131651) do

  create_table "battle_results", force: true do |t|
    t.integer  "battle_id"
    t.integer  "user_id"
    t.integer  "chips"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "battles", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "status"
  end

  create_table "cubes", force: true do |t|
    t.string   "name"
    t.integer  "health"
    t.integer  "generation_interval"
    t.string   "look_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "levels", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "loots", force: true do |t|
    t.string   "name"
    t.integer  "amount"
    t.integer  "generation_interval"
    t.integer  "min_pack_size"
    t.integer  "max_pack_size"
    t.string   "look_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "name",        null: false
    t.string   "email",       null: false
    t.string   "password",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sess_id"
    t.float    "speed"
    t.float    "jump_height"
    t.float    "capacity"
    t.float    "weight"
    t.integer  "expa"
    t.integer  "chips"
  end

  create_table "weapon_origins", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "weapons", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
