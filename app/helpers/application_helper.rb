module ApplicationHelper
  def link_to_sess_id body, url, *args
    if @sess_id
      url += (url.index("?") ? "&sess_id=" : "?sess_id=") + @sess_id
    end
    link_to body, url, *args #response.body
  end
end
