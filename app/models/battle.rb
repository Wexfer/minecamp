class Battle < ActiveRecord::Base
  
  def close
    self.status = 1
  end
  
  def is_closed?
    self.status == 1
  end
  
  has_many :battle_results
  belongs_to :admin, foreign_key: "user_id", class_name: "User"
  
end
