class User < ActiveRecord::Base
  validates :name, length: {minimum: 3, maximum: 20},
                   uniqueness: {case_sensitive: false},
                   if: :name
  validates :email, format: /\w+@\w+/,#email: true,
                    uniqueness: {case_sensitive: false},
                    if: :email
  validates :password, length: {minimum: 5, maximum:20}
  
  #has_one :player
  has_many :battles
  
#  def admin?
#    false
#  end
end
