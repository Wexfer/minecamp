class BattleResultsController < ApplicationController
  require 'redis'

  before_action :require_login, only: [:create]
  
  def initialize
    @redis = Redis.new
  end

  def test
    #render text: 'test'
  end
  
  def index
    if uid = params["user"]
      @user = User.find(uid)
      @battle_results = BattleResult.joins(:user).where({ user: @user }).all()
    else
      @battle_results = BattleResult.all()
    end
  end
  
  def create
    if not battle_id = params["id"]
      error = "no battle id"
    elsif not battle = Battle.where(id: battle_id).first
      error = "no battle with id="+battle_id
    elsif @current_user != battle.admin
      error = "you are not battle creator"
    elsif battle.is_closed?
      error = "battle already closed"
#    elsif amount = params["amount"] #user sended info about self
#      BattleResult.create(
#        battle_id: battle_id,
#        user_id: @current_user.id,
#        chips: amount
#      )
    else #received user array
      error = ""
      
      battle.close
      battle.save
      
      params.each do |uname,amount|
        unless uname.starts_with? "user" then next end
        uid = uname[4..-1]
        p uid, amount
        BattleResult.create(
          battle_id: battle_id,
          user_id: uid,
          chips: amount
        )
        u = User.find(uid)
        u.chips += amount
        u.save()
      end
    end
    respond_to do |format|
      format.json do
        render json: {error: error}
      end
    end
  end
  
end
