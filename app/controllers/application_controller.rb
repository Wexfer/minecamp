class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
  
  before_action :get_sess_id
  #before_action :require_login#, only: [:show]
  
  
  def get_sess_id
    @sess_id = params[:sess_id]
    @current_user = User.where(sess_id: @sess_id).first if @sess_id else nil
  end
  
  def require_login
    #get_sess_id()
    respond_to do |format|
      format.html do
        unless @current_user
          #redirect_to action: "user#index"
          render text: "not authenticated"
          return
        end
      end
      format.json do
        unless @current_user
          render text: '{"error": "not authenticated"}'
          return
        end
      end
    end
  end
  
  def get_new_sess_id
    rand.to_s#"The Very Random Token "+
  end
  
  def login(user)
    @sess_id = get_new_sess_id
    @current_user = user
    user.sess_id = @sess_id
    user.save()
  end
  
#  def current_user
#    @current_user = User.find(1)
#  end
end
