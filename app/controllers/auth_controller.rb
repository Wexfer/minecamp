class AuthController < ApplicationController
  
  def new
    
  end
  
  def create
    par = params[:user]
    @error = nil
    @user = User.where(name: par[:name]).first
    if @user
      if @user.password != par[:password]
      then @error = "Wrong password"
      else login @user end
    else
      @error = "No such user"
    end
    
    
    respond_to do |format|
      format.html {
        unless @error
          redirect_to '/?sess_id='+@sess_id
        else
          render action: "new"
        end
      }
      format.json {
        unless @error
          render json: { id: @current_user.id, sess_id: @sess_id }
        else
          render json: { error: @error }
        end
      }
    end
  end
  
end
