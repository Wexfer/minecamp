class BattlesController < ApplicationController
  before_action :require_login, only: [:create]
  
  def index
    
  end
  
  def create
    b = Battle.create(admin: @current_user)
    respond_to do |format|
      format.json { render json: {id: b.id} }
    end
  end
  
end
