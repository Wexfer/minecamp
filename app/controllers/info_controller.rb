class InfoController < ApplicationController
  before_action :require_login#, only: [:show]
  
  def show
    respond_to do |format|
      format.json do
        @user_data = @current_user.serializable_hash.except("password", "sess_id")
        render json: @user_data
      end
    end
  end
  
end
