class UsersController < ApplicationController
  
  def index
    @users = User.all
  end
  
  def show
    respond_to do |format|
      @user = User.find(params[:id])
      format.html
      format.json { render :json => { id: @user.id, name: @user.name } }
      #render json: @statuses.select([:latitude, :longitude, :timestamp, :virtual_odometer])
    end
  end
  
  
  def create
    @user = User.new(params[:user].permit(:name, :email, :password))
    respond_to do |format|
      format.html do
        if @user.save
          login @user
          redirect_to '/?sess_id='+@sess_id
        else
          render action: "new"
        end
      end
      format.json do
        if @user.save
          login @user
          render json: { id: @current_user.id, sess_id: @sess_id }
        else
          render json: @user.errors.messages
        end
      end
    end
  end
  
  def new
    @user = User.new
  end
  
end
